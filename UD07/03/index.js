class EstacionEspacial{
    superficie = Array()
    sucio = "X"
    limpio = "L"
    lineas
    columnas
    /*
    rrusas = Array([])
    ramericas = Array([])
    reuropeas = Array([])
    */
    rrusas = Array()
    ramericas = Array()
    reuropeas = Array()
    
    constructor (n,m, rrusas, ramericanas, reuropeas){
        if (rrusas + ramericanas + reuropeas > n){
            throw "Demasiadas rumbas o poca estacion espacial"
        }
        this.lineas = n
        this.columnas = m
        let linea = this.sucio.repeat(n)
        for (let i = 0;i<m; i++){
            let tmp = new Array()
            tmp = linea.split("")
            this.superficie.push(tmp)
        }
        this.anyaderusas(rrusas);
        this.anyadeamericanas(ramericanas);
        this.anyadeeuropeas(reuropeas);
    }
    anyaderusas(rrusas){
        for (let i = 0;i<rrusas;i++){
            let rumbarusa = new RoombaRusa()
            //this.rrusas[0].push(rumbarusa.id)
            this.rrusas.push(rumbarusa)
            this.superficie[rumbarusa.posY][rumbarusa.posX] = this.limpio
        }
    }
    anyadeamericanas(ramericanas){
        for (let i = 0;i<ramericanas;i++){
            let rumbaamerica = new RoombaAmericana()
            //this.ramericas[0].push(rumbaamerica.id)
            this.ramericas.push(rumbaamerica)
            this.superficie[rumbaamerica.posY][rumbaamerica.posX] = this.limpio
        }
    }
    anyadeeuropeas(reuropeas){
        for (let i = 0;i<reuropeas;i++){
            let rumbaeuropea = new RoombaEuropea()
            //this.reuropeas[0].push(rumbaeuropea.id)
            this.reuropeas.push(rumbaeuropea)
            this.superficie[rumbaeuropea.posY][rumbaeuropea.posX] = this.limpio
        }
    }
    casillaLimpia(elemento){
        return elemento === "L"
    }
    todaLimpia(){
        let tlimpio = true
        this.superficie.forEach(element => {
            if (!element.every(this.casillaLimpia)){
                tlimpio = false
            }
        });
        return tlimpio
    }
    moverRoombas(){
        this.rrusas.forEach(element => {
            let mov = element.movimiento();
            element.posX = element.posX+mov[0] === this.lineas ? 0 : element.posX+mov[0]
            element.posX = element.posX === -1 ? this.lineas -1 : element.posX
            element.posY = element.posY+mov[1] === this.columnas ? 0 : element.posY+mov[1]
            element.posY = element.posY === -1 ? this.columnas -1 : element.posY
            this.superficie[element.posY][element.posX] = this.limpio
        });

        this.ramericas.forEach(element => {
            let mov = element.movimiento();
            element.posX = element.posX+mov[0] === this.lineas ? 0 : element.posX+mov[0]
            element.posX = element.posX === -1 ? this.lineas -1 : element.posX
            element.posY = element.posY+mov[1] === this.columnas ? 0 : element.posY+mov[1]
            element.posY = element.posY === -1 ? this.columnas -1 : element.posY
            this.superficie[element.posY][element.posX] = this.limpio
        });

        this.reuropeas.forEach(element => {
            let mov = element.movimiento();
            element.posX = element.posX+mov[0] === this.lineas ? 0 : element.posX+mov[0]
            element.posX = element.posX === -1 ? this.lineas -1 : element.posX
            element.posY = element.posY+mov[1] === this.columnas ? 0 : element.posY+mov[1]
            element.posY = element.posY === -1 ? this.columnas -1 : element.posY
            this.superficie[element.posY][element.posX] = this.limpio
        });
    }

}


class Roomba{
    posX
    posY = 0 
    id 
    static #inicial = -1
    static #id = 100
    tiempoEspera = 0
    fabricadoEN = "china"
    constructor (){
        this.posX = Roomba.#inicial++;
        this.id = Roomba.#id++;
    }
    getRandomInt() {
        return Math.floor(Math.random()*2);
      }

    movimiento(){
        let mov = Array([0,0])
        let tmp
        tmp = this.getRandomInt()
        mov[0] = tmp===0 ? -1 : 1;
        tmp = this.getRandomInt()
        mov[1] = tmp===0 ? -1 : 1;
        return mov
    }
}

class RoombaAmericana extends Roomba {
    choque(){
        return [1,0]
    }
}

class RoombaRusa extends Roomba {
    choque(){
        return [-1,0]
    }
}

class RoombaEuropea extends Roomba {
    choque(){
        this.tiempoEspera = 2
    }
}


let c 
c = new Roomba

let est
est = new EstacionEspacial(20, 5, 3, 3, 3)



let contador = 0
while (!est.todaLimpia() && contador < 250){
    est.moverRoombas()    
    contador++
}

console.log (est.superficie)
console.log (contador)
