class ProcesadorTexto{
    ancho
    parrafo = Array()
    defecto = "."
    constructor(ancho){
        this.ancho = ancho;
    }
    bloque(texto, alineacion){
        let lineas = Math.ceil(texto.length / this.ancho)
        for (let i = 0; i< lineas -1; i++){
            let subCadena = texto.slice(i*this.ancho, (i+1)*this.ancho)
            subCadena = subCadena.split("") 
            this.parrafo.push(subCadena)
        }
        if (texto.length > ((lineas * this.ancho)- this.ancho) ){
            if (alineacion === "izq"){
                this.parrafo.push(this.aliniacionizquierda(texto.slice( (lineas*this.ancho)-this.ancho, texto.length)))
            }
            else if(alineacion === "der"){
                this.parrafo.push(this.aliniacionderecha(texto.slice( (lineas*this.ancho)-this.ancho, texto.length)))
            }
            else if(alineacion === "cen"){
                this.parrafo.push(this.aliniacioncentro(texto.slice( (lineas*this.ancho)-this.ancho, texto.length)))
            }
        }
    }
    aliniacionizquierda(texto){
        let puntos = this.ancho - texto.length
        let linea = texto + this.defecto.repeat(puntos)
        linea = linea.split("")
        return  linea
    }
    aliniacionderecha(texto){
        let puntos = this.ancho - texto.length
        let linea = this.defecto.repeat(puntos) + texto
        linea = linea.split("")
        return  linea
    }
    aliniacioncentro(texto){
        let puntos = this.ancho - texto.length
        let linea = this.defecto.repeat(Math.ceil(puntos / 2 )) + texto + this.defecto.repeat(Math.ceil(puntos / 2 ))
        linea = linea.split("")
        return  linea
    }
    get parrafo (){
        return this.parrafo
    }
    
}

let pro
pro = new ProcesadorTexto(15)
pro.bloque("hola mundo 1", "izq")
pro.bloque("hola mundo 2", "der")
pro.bloque("hola mundo 3", "cen")

console.log(pro.parrafo)