let direcciones = [
    {
    // Válido
    pais: 'España', region: '', cp: '46014',
    ciudad: 'Valencia', direccion: 'Carrer Misericòrdia, 34',
    complemento: '',
    movil: '', fijo: '961 20 69 90'
    },
    {
    // Inválido: no tiene movil o fijo
    pais: 'España', region: '', cp: '46960',
    ciudad: 'Aldaia', direccion: 'C/ Montcabrer, 22',
    complemento: 'Pol. Ind. La Lloma',
    movil: '', fijo: ''
    },
    {
    // Inválido: no tiene país
    pais: '', region: 'Alicante', cp: '',
    ciudad: 'Petrer', direccion: 'Los Pinos, 7',
    complemento: '',
    movil: '', fijo: '965 37 08 88'
    }
    ]

function pipe (...functions){
    return function (param) {
        let result = functions[0](param);
        for (let i=1 ; i<functions.length; i++){
            result = functions[i](result)
        }
        return result
    }
}

let paisCiudadDireccion = direc => !!direc.pais;
let movilTelefono = direc => !!direc.movil || !!direc.fijo;
let regionCP = direc => !!direc.region || !!direc.cp;

const comp = function (direc){
    let valido
    valido = paisCiudadDireccion(direc)
    valido = valido && movilTelefono(direc)
    valido = valido && regionCP(direc)
    return valido
}


let res = direcciones.filter(elemento => comp(elemento))
console.log (res)
