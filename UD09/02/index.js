/*let map = function(a, f){
    console.log (a.map(elemento => f(elemento)))
}
map([1,2,3], x => x * 2) // [2,4,6]
*/

function map (a, f){
    let devolver = Array()
    a.reduce((_, valor) => devolver.push(f(valor)),0 );
    return devolver
}
console.log(map([1,2,3], x => x * 2)) // [2,4,6]