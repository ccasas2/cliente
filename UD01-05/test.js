
const HE = ' '
const HF = ' — '
const VL = '| '
const VR = ' |'
const VF = '| |'

const CERO = [HF, VF, HE, VF, HF]
const UNO = [HE, VR, HE, VR, HE]
const DOS = [HF, VR, HF, VL, HF]
const TRES = [HF, VR, HF, VR, HF]
const CUATRO = [HE, VF, HF, VR, HE]
const CINCO = [HF, VL, HF, VR, HF]
const SEIS = [HF, VL, HF, VF, HF]
const SIETE = [HF, VR, HE, VR, HE]
const OCHO = [HF, VF, HF, VF, HF]
const NUEVE = [HF, VF, HF, VR, HF]

const MisNumerosPlantilla = [CERO, UNO, DOS, TRES, CUATRO, CINCO, SEIS, SIETE, OCHO, NUEVE]
const MAX_NUM_LCD = 999999
const MIN_NUM_LCD = 1
let msgFueraDeRango = `Número fuera de rango: ${MIN_NUM_LCD} - ${MAX_NUM_LCD}, no se admiten negativos`
let isNUmeroDentroRango = (num) => num < MAX_NUM_LCD && num >= MIN_NUM_LCD

function muestraLCD(...leds) {
let tamanio = leds[0].length
for (let i = 0; i < 5; i++) {
let presentacion = ""
for (let j = 0; j < tamanio; j++) {
if (tamanio >= 0)
presentacion += leds[0][j][i] + " "
}
console.log(presentacion)
}
}
function createDigitosLCD(arrayDeNumeros) {
let MiArrayLeds = []
for (const element of arrayDeNumeros) {
MiArrayLeds.push(MisNumerosPlantilla[element])
}
muestraLCD(MiArrayLeds)
}


function lcd(num) {
if (isNUmeroDentroRango(num)) {
let cadenaNumeros = num.toString().split("")
createDigitosLCD(cadenaNumeros)
} else {
console.log(msgFueraDeRango)
}
}

lcd(547824)
