
function combineArrays(a1, a2){
    let combinado = []
    combinado.push(...a2, ...a1)
    //combinado = a2.concat(a1)
    return combinado
}
console.log(combineArrays([1,2], [3,4])) // => [3,4,1,2]