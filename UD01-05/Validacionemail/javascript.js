    let listaServidores=[ "terra.es" , "myspace.com", "arrakis.es", "tuenti.es"];
    function validapartesemail(parte1, parte2){
        if (parte1 === ""){
            return false
        }
        let parte2trozos = parte2.split(".")
        if (parte2trozos.length <= 1){
            return false
        }

        if (!listaServidores.includes(parte2trozos[parte2trozos.length-2] + "." + parte2trozos[parte2trozos.length-1])){
            return false
        }
        return true
    }

    function marcarinvalido (target){
        target.classList.add ("invalido")
    }
    function marcarvalido (target){
        target.classList.remove ("invalido")
    }

    function validaemail(target){
        let partesemail = target.value.split("@")
        if (partesemail.length === 2){
            if (validapartesemail(partesemail[0], partesemail[1])){
                marcarvalido(target)
                return true
            }
        }            
        marcarinvalido(target)
        return false
    }
    
    

    // hasta aqui el ejercicio anterior todo exactamente igual

    function comparaemails(email1, email2){
        if (email1.value === email2.value){
            return true
        }
        console.log("diferentes")
        return false
    }

    function validarformulario(event){
        event.preventDefault()
        let email1 = document.getElementById("email");
        let email2 = document.getElementById("emailrep");
        //validaciones individuales
        let todocorrecto = true;
        todocorrecto = todocorrecto && validaemail(email1)
        todocorrecto = todocorrecto && validaemail(email2)
        todocorrecto = todocorrecto && comparaemails(email1, email2)
        console.log(todocorrecto)
        if (todocorrecto){
            document.forms[0].submit()
        }
    }

    document.getElementById("bsubmit").addEventListener("click", function(event){
        validarformulario(event)
      });

      //ejercicio 3
      const ciudades = {Alicante:["Alicante capital", "Elche", "Orihuela"], 
                        Valencia:["Valencia capital", "Torrent", "Mislata", "Paiporta"],
                        Castellon:["Castellon de la plana", "Oropesa", "Vinaroz"]}
      const ciudadesdefecto = {Alicante:"", 
                               Valencia:"Mislata",
                               Castellon:""}


      function cambiociudad(){
        document.getElementById("ciudades").innerHTML ="";
        let htmlciudades = document.getElementById("ciudades")

        let option = document.createElement("option");
        option.text = "Seleeciona una ciudad";
        option.value = "none";
        option.disabled = true;
        option.hidden = true;
        option.selected = true;
        htmlciudades.add(option);
        const nprovincia = document.getElementById("provincias").value
        for (const ciudad in ciudades[document.getElementById("provincias").value]) {
            option = document.createElement("option");
            option.text = ciudades[nprovincia][ciudad];
            option.value = ciudades[nprovincia][ciudad];
            htmlciudades.add(option);

          }
        if (ciudadesdefecto[nprovincia] != ""){
            htmlciudades.value = ciudadesdefecto[nprovincia];
        }


      }
      


