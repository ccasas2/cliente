let numeros= {"1":"CF", "2":"ACDEG", "3":"ACDFG", "4":"BCDF", "5":"ABDFG", "6":"ABDFGE", "7":"ACF", "8":"ABCDEFG", "9":"ABCDF", "0":"ABCEFG"}

function primeralinea(numero) {
    let linea = ""
    for (let i= 0; i< numero.length; i++){ //cada uno de los numeros
        if ( numeros[numero.charAt(i)].search("A") != -1){
            linea += " -  "
        }
        else{
            linea += "    "
        }
    }
    return (linea)
}

function segundalinea(numero) {
    let linea = ""
    for (let i= 0; i< numero.length; i++){ //cada uno de los numeros
        if ( numeros[numero.charAt(i)].search("B") != -1){
            linea += "| "
        }
        else{
            linea += "  "
        }
        if ( numeros[numero.charAt(i)].search("C") != -1){
            linea += "| "
            
        }
        else{
            linea += "  "
        }
    }
    return (linea)
}

function terceralinea(numero) {
    let linea = ""
    for (let i= 0; i< numero.length; i++){ //cada uno de los numeros
        if ( numeros[numero.charAt(i)].search("D") != -1){
            linea += " -  "
        }
        else{
            linea += "    "
        }
    }
    return (linea)
}

function cuartalinea(numero) {
    let linea = ""
    for (let i= 0; i< numero.length; i++){ //cada uno de los numeros
        if ( numeros[numero.charAt(i)].search("E") != -1){
            linea += "| "
        }
        else{
            linea += "  "
        }
        if ( numeros[numero.charAt(i)].search("F") != -1){
            linea += "| "
            
        }
        else{
            linea += "  "
        }
    }
    return (linea)
}

function quintalinea(numero) {
    let linea = ""
    for (let i= 0; i< numero.length; i++){ //cada uno de los numeros
        if ( numeros[numero.charAt(i)].search("G") != -1){
            linea += " -  "
        }
        else{
            linea += "    "
        }
    }
    return (linea)
}

function pintalcd (num){
    let numero = num.toString()
    console.log(primeralinea(numero))
    console.log(segundalinea(numero))
    console.log(terceralinea(numero))
    console.log(cuartalinea(numero))   
    console.log(quintalinea(numero))
}


pintalcd (1234567890)