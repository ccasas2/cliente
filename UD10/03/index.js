const asyncRequest = require("../asyncRequest")

function cutriFetch(nombreRecurso){
    return new Promise((resolve, reject)=> {
        asyncRequest (nombreRecurso, (dato) => resolve(dato))
    })
}

cutriFetch("resource1")
.then((datos) =>{console.log("primera:  " + datos)
                return cutriFetch("resource2")}
)
.then((datos2) =>{console.log("segunda: " + datos2)
                return cutriFetch("resource3")}
)
.then((datos3) =>{console.log("tercera: " +datos3)
                  console.log("!Completado¡")} 
)
