const asyncRequest = require("../asyncRequest")

asyncRequest ("resource1", (datos) =>  {
    console.log('primera: ' + datos);
    asyncRequest ("resource2", (datos2) => {
        console.log("segunda: " + datos2);
        asyncRequest ("resource3", (datos3) => {
            console.log("tercera: " + datos3);
            console.log("!Completado¡")
        })
    })
})