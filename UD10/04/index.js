let total = 301

let valorImprimir = Array()
valorImprimir[total] = "buzz"
let mayorImpreso = 0

function esFizz (num){
    if (num % 3 === 0 || num.toString().includes("3")){
        return "fizz"
    }
    return num
}

function fizz(num, callback){
    const MIN_DELAY = 10;
    const RANDOM_DELAY = 20;
    var randomDelay = Math.round(Math.random() * RANDOM_DELAY) + MIN_DELAY;
    setTimeout(function(){
        callback(num)
    }
    ,randomDelay);
}

function guardaResultado (posicion, valor){
    valorImprimir[posicion] = valor;
}
function imprimeResultados (){
   for (let i = mayorImpreso+1 ; i<= total -1; i++){
        if (valorImprimir[i]){
            console.log(i + "--->" + valorImprimir[i])
            mayorImpreso = i;
        }
        else{return}
   }
}

function llamadasFizz(num){
    return new Promise((resolve, reject)=> {
        fizz (num, (dato) => resolve(dato))
    })
}

for (let i=0; i<total;i++){
    llamadasFizz(i)
    .then((dato) =>{return guardaResultado(i,esFizz(dato))}
    )
    .then(()=>imprimeResultados())    
}