function lazyMultiply(n1, n2){
    let dosvalores = n1 && n2 
    if (dosvalores){
        return n1 * n2
    }
    else if (n1){
        let f = function (n){
            return n*n1
        }
        return f
    }
    else{
        throw "minimo un valor"
    }
}


lazyMultiply(7,4) // 28
const perTwo = lazyMultiply(2)
perTwo(3) // 6
lazyMultiply(5)(10) // 50