const MINIMUM = 15;
function mergeValues(arrayOfIntegers) {
    let element;
    let sum = 0;
    let copuiaArray = Array()
    arrayOfIntegers.forEach(element => {
        sum += element
    });
    sum = Math.max(sum, MINIMUM)
    copuiaArray.push(sum);
    return copuiaArray;
}
let esteNoSeModifica = [10,20,30,40]
console.log(mergeValues(esteNoSeModifica)) // [100]
console.log(esteNoSeModifica)
let esteNoSeModifica2 = [1,2,3,4]
console.log(mergeValues(esteNoSeModifica2)) // [15] (MINIMUM)
console.log(esteNoSeModifica2)