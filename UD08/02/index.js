function createIDGenerator(caracteres){
    //let id = "0".repeat(caracteres)
    let idGlobal = 0
    let f =  function (){
        idGlobal ++
        let idtmp = "" + idGlobal
        let ceros = caracteres - idtmp.length
        const id = "0".repeat(ceros) + idGlobal
        console.log (id)
    }
    return f
    
}

const len3Id = createIDGenerator(3);
len3Id() // 001
len3Id() // 002
len3Id() // 003
const len5Id = createIDGenerator(5);
len5Id() // 00001