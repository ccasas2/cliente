function doublePipe (){
    let funciones = {arguments}
    let f = function (n){
        for (elemento of funciones["arguments"]){
            n = elemento(n)
            n = elemento(n)
        }
        return n
    }
    return f
}

function double(x) { return x*2 }
function add3(x) { return x+3 }
let multiplyPerFourAndAddSix = doublePipe(double, add3)
console.log(multiplyPerFourAndAddSix(10)) // 46 = (10*2*2+3+3)
let addSixAndMultiplyPerFour = doublePipe(add3, double)
console.log(addSixAndMultiplyPerFour(10)) // 64 = (10+3+3)*2*2