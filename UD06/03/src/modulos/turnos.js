let jugador1 = "X"
let jugador2 = "O"

let jugador_actual = jugador1;

function cambioJugador(){
    if (jugador_actual === jugador1){
        jugador_actual = jugador2;
    }
    else{
        jugador_actual = jugador1;
    }
}

export {jugador_actual, cambioJugador}