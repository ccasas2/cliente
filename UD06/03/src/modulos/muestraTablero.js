import { tablero } from "./tablero";

function muestraTablero(){
    let tableroactual = ""
    for (let i = 0; i< tablero.length;i++){
        let linea = ""
        for (let j = 0; j< tablero[i].length;j++){
            linea += tablero[i][j] + "|";
        }
        tableroactual += linea.substring(0, linea.length -1) + "\n"
    }
    console.log(tableroactual)
    
}

export {muestraTablero}